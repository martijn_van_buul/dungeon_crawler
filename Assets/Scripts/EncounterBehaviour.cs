﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterBehaviour : MonoBehaviour
{
    private Encounter myEncounter;


    public void InitializeEncounter(Encounter newEncounter = null)
    {
        if (newEncounter != null)
            myEncounter = newEncounter;

        GetComponent<SpriteRenderer>().sprite = myEncounter.sprite;
    }
}
