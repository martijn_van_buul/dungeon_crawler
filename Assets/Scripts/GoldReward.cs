﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldReward : Reward
{
    public int MinAmount;
    public int MaxAmount;

    public int GetAmountGold()
    {
        return Random.Range(MinAmount, MaxAmount);
    }
}
