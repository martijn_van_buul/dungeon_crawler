﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class CreateScriptableObject : MonoBehaviour
{
    [MenuItem("Assets/Create/Spell")]
    public static void CreateSpellAsset()
    {
        Spell asset = ScriptableObject.CreateInstance<Spell>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Spells/new Spell.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/Reward/Gold")]
    public static void CreateGoldRewardAsset()
    {
        GoldReward asset = ScriptableObject.CreateInstance<GoldReward>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Rewards/new GoldReward.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Character")]
    public static void CreateCharacterAsset()
    {
        Character asset = ScriptableObject.CreateInstance<Character>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Characters/new Character.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }


    [MenuItem("Assets/Create/Dice")]
    public static void CreateDiceAsset()
    {
        Die asset = ScriptableObject.CreateInstance<Die>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Dice/new Die.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Spellcaster/Player")]
    public static void CreatePlayerAsset()
    {
        Player asset = ScriptableObject.CreateInstance<Player>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Spellcasters/new Player.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Spellcaster/Monster")]
    public static void CreateMonsterAsset()
    {
        Enemy asset = ScriptableObject.CreateInstance<Enemy>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Spellcasters/Enemies/new Enemy.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif