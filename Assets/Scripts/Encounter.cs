﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encounter : ScriptableObject
{
    public Sprite sprite;

    public Vector3 position;
    public Color color;


    public List<Encounter> previousEncounters = new List<Encounter>();
    public List<Encounter> nextEncounters = new List<Encounter>();
}
