﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellVisualInitialiser : MonoBehaviour
{
    [SerializeField]
    SpellCaster caster;
    [SerializeField]
    GameObject spellObjectPrefab;
    [SerializeField]
    DieVisualInitialiser diceVisualiser;

    void Start()
    {
        foreach(Spell spell in caster.GetSpells())
        {
            GameObject spellObject = Instantiate(spellObjectPrefab, transform.GetChild(0));
            spellObject.GetComponent<SpellVisual>().Initialize(spell);
            spellObject.transform.GetComponentInChildren<Button>().onClick.AddListener(delegate { BattleManager.instance.CastSpell(spell, caster); });
            spellObject.transform.GetComponentInChildren<Button>().onClick.AddListener(delegate { ToggleSpellWindow(false); });
            spellObject.transform.GetComponentInChildren<Button>().onClick.AddListener(delegate { diceVisualiser.ToggleDiceWindow(true); });
        }
    }

    public void ToggleSpellWindow(bool activated)
    {
        transform.GetChild(0).gameObject.SetActive(activated);
    }
}
