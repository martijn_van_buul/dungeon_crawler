﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpellVisual : MonoBehaviour
{
    [SerializeField]
    Image spellImage;
    [SerializeField]
    TMP_Text nameText;
    [SerializeField]
    TMP_Text minDiceText;
    [SerializeField]
    TMP_Text maxDiceText;

    private Spell mySpell;

    public void Initialize(Spell spell)
    {
        mySpell = spell;

        nameText.text = spell.DisplayName;
        spellImage.sprite = spell.SpellSprite;
        minDiceText.text = spell.MinDice.ToString();
        maxDiceText.text = spell.MaxDice.ToString();
    }
}
