﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceGenerator : MonoBehaviour
{
    public static DiceGenerator instance;

    private void Awake()
    {
        instance = this;
    }

    public List<int> RollDice(List<Die> die)
    {
        List<int> results = new List<int>();

        foreach (Die dice in die)
            results.Add(dice.GetValues()[Random.Range(0, dice.GetFaceCount())]);

        return results;
    }

    public Die CreateDice(int faces)
    {
        Die newDice = ScriptableObject.CreateInstance<Die>();
        newDice.Initialize(faces);

        return newDice;
    }
}
