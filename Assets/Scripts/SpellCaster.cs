﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCaster : ScriptableObject
{
    [SerializeField]
    protected string displayName = "Monster";

    [SerializeField]
    protected List<Spell> mySpells = new List<Spell>();
    [SerializeField]
    protected List<Die> myDice = new List<Die>();

    private bool hasCasted;

    [SerializeField]
    int maxHealth;
    [SerializeField]
    int currentHealth;

    public Sprite CasterImage;

    public delegate void HealthUpdated(SpellCaster caster);
    public HealthUpdated onHealthUpdated;

    public List<Spell> GetSpells()
    {
        return mySpells;
    }

    public bool GetHasCasted()
    {
        return hasCasted;
    }

    public void SetHasCasted(bool hasCasted)
    {
        this.hasCasted = hasCasted;
    }


    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public bool TakeDamage(int damageAmount)
    {
        currentHealth = Mathf.Clamp(currentHealth - damageAmount, 0, maxHealth);

        if (onHealthUpdated != null)
            onHealthUpdated(this);

        return currentHealth == 0;
    }

    public bool Heal(int healAmount)
    {
        currentHealth = Mathf.Clamp(currentHealth + healAmount, 0, maxHealth);

        if (onHealthUpdated != null)
            onHealthUpdated(this);

        return currentHealth == maxHealth;
    }

    public List<Die> GetDice()
    {
        return myDice;
    }

    public Sprite GetCasterImage()
    {
        return CasterImage;
    }

    public void SetCasterImage(Sprite casterImage)
    {
        CasterImage = casterImage;
    }
}
