﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BattleManager : MonoBehaviour
{
    public static BattleManager instance;

    [SerializeField]
    List<Player> players;
    [SerializeField]
    List<Enemy> enemies;
    [SerializeField]
    List<SpellVisualInitialiser> spellVisualisers;

    List<SpellCaster> allCasters = new List<SpellCaster>();

    List<KeyValuePair<Spell, int>> spellAndSpellvalues = new List<KeyValuePair<Spell, int>>();
    List<KeyValuePair<SpellCaster, Die>> castersAndDice = new List<KeyValuePair<SpellCaster, Die>>();

    List<KeyValuePair<SpellCaster, bool>> castersAndDiceThrown = new List<KeyValuePair<SpellCaster, bool>>();


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        InitializeBattle();
    }

    private void InitializeBattle()
    {
        allCasters.AddRange(players);
        allCasters.AddRange(enemies);

        foreach(SpellCaster caster in allCasters)
        {
            caster.Heal(caster.GetMaxHealth());
        }
    }

    public void CastSpell(Spell spell, SpellCaster caster)
    {
        Spell spellCopy = Instantiate(spell);
        spellCopy.Owner = caster;
        spellCopy.Target = allCasters.First(spellCaster => spellCaster != caster);


        spellAndSpellvalues.Add(new KeyValuePair<Spell, int>(spellCopy, 0));
    }

    public void ToggleDice(Die die, SpellCaster caster)
    {
        //if (castersAndDice.Count(pair => pair.Value == die) == 0)
        castersAndDice.Add(new KeyValuePair<SpellCaster, Die>(caster, die));
        //else
        //    castersAndDice.RemoveAll(pair => pair.Value == die);
    }

    public void CasterDiceDone(SpellCaster caster)
    {
        castersAndDiceThrown.Add(new KeyValuePair<SpellCaster, bool>(caster, true));
        caster.SetHasCasted(true);

        if (castersAndDiceThrown.Count == allCasters.Count)
            ResolveDice();
    }

    private void ResolveDice()
    {
        List<KeyValuePair<Spell, int>> tempSpellAndSpellvalues = new List<KeyValuePair<Spell, int>>();

        foreach (KeyValuePair<Spell, int> pair in spellAndSpellvalues)
        {
            int value = 0;

            foreach (KeyValuePair<SpellCaster, Die> dicePair in castersAndDice.Where(casterDicePair => casterDicePair.Key == pair.Key.Owner))
                value += dicePair.Value.GetValues()[Random.Range(0, dicePair.Value.GetValues().Count)];

            tempSpellAndSpellvalues.Add(new KeyValuePair<Spell, int>(pair.Key, value));
        }


        spellAndSpellvalues = tempSpellAndSpellvalues;

        if (allCasters.Count(spellCaster => spellCaster.GetHasCasted()) == allCasters.Count)
            ResolveSpells();
    }

    private void ResolveSpells()
    {
        foreach (KeyValuePair<Spell, int> pair in spellAndSpellvalues)
        {
            bool targetDies = pair.Key.Target.TakeDamage(pair.Value);

            if (targetDies && pair.Key.Target is Enemy)
                players[0].AddReward(((Enemy)pair.Key.Target).GetReward());
        }

        spellAndSpellvalues.Clear();
        castersAndDice.Clear();
        castersAndDiceThrown.Clear();

        foreach (SpellCaster caster in allCasters)
            caster.SetHasCasted(false);

        foreach (SpellVisualInitialiser visualiser in spellVisualisers)
            visualiser.ToggleSpellWindow(true);
    }
}
