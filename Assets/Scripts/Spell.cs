﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : ScriptableObject
{
    public string DisplayName;
    public E_Color myColor;
    public int MinDice;
    public int MaxDice;
    public Sprite SpellSprite;
    public SpellCaster Target;
    public SpellCaster Owner;
}
