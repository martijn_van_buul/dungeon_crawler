﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : SpellCaster
{
    [SerializeField]
    List<Artifact> myArtifacts = new List<Artifact>();
    [SerializeField]
    int gold;

    void Start()
    {

    }

    public void AddReward(Reward newReward)
    {
        if (newReward is GoldReward)
            gold += ((GoldReward)newReward).GetAmountGold();
    }

    public void SetDice(List<Die> dice)
    {
        myDice = dice;
    }

    public void SetDisplayName(string newName)
    {
        displayName = newName;
    }
}
