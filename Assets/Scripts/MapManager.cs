﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapManager : MonoBehaviour
{
    private Encounter initialEncounter;

    [SerializeField]
    private GameObject encounterObject;
    [SerializeField]
    private Sprite testSprite;

    private List<KeyValuePair<Encounter, Vector2>> encounterLocations = new List<KeyValuePair<Encounter, Vector2>>();

    void Start()
    {
        GenerateMap();
    }

    private void GenerateMap()
    {
        Encounter initialEncounter = ScriptableObject.CreateInstance<Encounter>();

        GameObject newEncounterObject = Instantiate(encounterObject, transform);
        newEncounterObject.transform.localPosition = new Vector3(0, (-3) * 5, 0);

        newEncounterObject.GetComponent<EncounterBehaviour>().InitializeEncounter(initialEncounter);
        initialEncounter.color = Color.black;
        newEncounterObject.GetComponent<SpriteRenderer>().color = Color.black;

        for (int encounterLevel = 0; encounterLevel < 5; encounterLevel++)
        {
            for(int encounterLane = -2; encounterLane <= 2; encounterLane++)
            {
                Encounter newEncounter = ScriptableObject.CreateInstance<Encounter>();
                newEncounter.sprite = testSprite;
                newEncounter.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1);

                newEncounterObject = Instantiate(encounterObject, transform);
                newEncounterObject.transform.localPosition = new Vector3(encounterLane * 5, (encounterLevel - 2) * 5 + Mathf.Abs(encounterLane), 0);

                newEncounterObject.GetComponent<EncounterBehaviour>().InitializeEncounter(newEncounter);
                newEncounterObject.GetComponent<SpriteRenderer>().color = newEncounter.color;
                newEncounter.position = newEncounterObject.transform.localPosition;

                encounterLocations.Add(new KeyValuePair<Encounter, Vector2>(newEncounter, new Vector2(encounterLevel, encounterLane)));

                if (encounterLevel == 0)
                {
                    initialEncounter.nextEncounters.Add(newEncounter);
                    newEncounter.previousEncounters.Add(initialEncounter);
                }
            }
        }

        foreach(KeyValuePair<Encounter, Vector2> pair in encounterLocations)
        {
            for(int i = -1; i < 2; i++)
            {
                KeyValuePair<Encounter, Vector2> foundPair = encounterLocations.FirstOrDefault(possiblePair => possiblePair.Value == pair.Value + new Vector2(-1, i));

                if (foundPair.Key != null)
                {
                    if(!foundPair.Key.nextEncounters.Contains(pair.Key))
                        foundPair.Key.nextEncounters.Add(pair.Key);
                    if (!pair.Key.previousEncounters.Contains(pair.Key))
                        pair.Key.previousEncounters.Add(foundPair.Key);
                }

            }

            for (int i = -1; i < 2; i++)
            {
                KeyValuePair<Encounter, Vector2> foundPair = encounterLocations.FirstOrDefault(possiblePair => possiblePair.Value == pair.Value + new Vector2(0, i));

                if (foundPair.Key != null && Mathf.Abs(foundPair.Value.y) > Mathf.Abs(pair.Value.y))
                {
                    if (!foundPair.Key.previousEncounters.Contains(pair.Key))
                        foundPair.Key.previousEncounters.Add(pair.Key);
                    if (!pair.Key.nextEncounters.Contains(pair.Key))
                        pair.Key.nextEncounters.Add(foundPair.Key);
                }
            }

            for (int i = -1; i < 2; i++)
            {
                KeyValuePair<Encounter, Vector2> foundPair = encounterLocations.FirstOrDefault(possiblePair => possiblePair.Value == pair.Value + new Vector2(+1, i));

                if (foundPair.Key != null)
                {
                    if (!foundPair.Key.previousEncounters.Contains(pair.Key))
                        foundPair.Key.previousEncounters.Add(pair.Key);
                    if (!pair.Key.nextEncounters.Contains(pair.Key))
                        pair.Key.nextEncounters.Add(foundPair.Key);
                }
            }
        }
    }
}
