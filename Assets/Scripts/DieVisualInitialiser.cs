﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DieVisualInitialiser : MonoBehaviour
{
    [SerializeField]
    SpellCaster caster;
    [SerializeField]
    GameObject diceObjectPrefab;

    private bool initialisedDice;

    private void InitializeDice()
    {
        if (initialisedDice)
            return;

        initialisedDice = true;
        foreach (Die dice in caster.GetDice())
        {
            GameObject diceObject = Instantiate(diceObjectPrefab, transform.GetChild(0));
            diceObject.GetComponent<DieVisual>().Initialize(dice);

            diceObject.transform.GetComponentInChildren<Button>().onClick.AddListener(delegate { BattleManager.instance.ToggleDice(dice, caster); });
        }
    }

    public void ToggleDiceWindow(bool activated)
    {
        InitializeDice();
        transform.GetChild(0).gameObject.SetActive(activated);

        if (!activated)
            BattleManager.instance.CasterDiceDone(caster);
    }
}
