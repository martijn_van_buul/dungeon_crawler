﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_Color
{
    Red,
    Blue,
    Yellow,
    Purple,
    Green,
    Orange,
    White,
    Black
}

public static class DieUtil 
{
    public static Color getDiceColor(E_Color color)
    {
        switch (color)
        {
            case E_Color.Red:
                return Color.red;
            case E_Color.Blue:
                return Color.blue;
            case E_Color.Yellow:
                return Color.yellow;
            case E_Color.Purple:
                return new Color(0.5f, 0, 0.5f);
            case E_Color.Green:
                return Color.green;
            case E_Color.Orange:
                return new Color(1, 99/255f, 71f/255f);
            case E_Color.White:
                return Color.white;
            case E_Color.Black:
                return Color.black;
            default:
                return Color.white;
        }
    }
}

public class Die : ScriptableObject
{
    public List<int> myDiceValues = new List<int>();
    public E_Color myColor;

    public int lastValue = -1;

    public void Initialize(int amountSides)
    {
        for (int i = 1; i <= amountSides; i++)
            myDiceValues.Add(i);
    }

    public List<int> GetValues()
    {
        return myDiceValues;
    }

    public int GetFaceCount()
    {
        return myDiceValues.Count;
    }

    public E_Color GetColor()
    {
        return myColor;
    }

}