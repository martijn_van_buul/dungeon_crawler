﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : SpellCaster
{
    [SerializeField]
    List<Reward> potentialRewards = new List<Reward>();


    // Start is called before the first frame update
    void Start()
    {
        myDice.Add(DiceGenerator.instance.CreateDice(4));
        myDice.Add(DiceGenerator.instance.CreateDice(6));
    }

    public Reward GetReward()
    {
        return potentialRewards[Random.Range(0, potentialRewards.Count)];
    }
}
