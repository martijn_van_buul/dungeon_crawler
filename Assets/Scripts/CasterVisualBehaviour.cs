﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CasterVisualBehaviour : MonoBehaviour
{
    Image fillBar;
    TMP_Text healthText;

    [SerializeField]
    SpellCaster caster;
    [SerializeField]
    Image spellCasterImage;

    // Start is called before the first frame update
    void Start()
    {
        fillBar = transform.GetChild(0).GetComponent<Image>();
        healthText = transform.GetChild(1).GetComponent<TMP_Text>();
        caster.onHealthUpdated += UpdateBar;
        spellCasterImage.sprite = caster.GetCasterImage();

        UpdateBar(caster);
    }

    private void UpdateBar(SpellCaster caster)
    {
        fillBar.fillAmount = ((float)caster.GetCurrentHealth()) / ((float)caster.GetMaxHealth());
        healthText.text = caster.GetCurrentHealth().ToString() + "/" + caster.GetMaxHealth().ToString();
    }
}
