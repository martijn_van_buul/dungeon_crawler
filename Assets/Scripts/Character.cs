﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_Race
{
    Dwarf,
    Human
}


public enum E_Class
{
    Warrior,
    Priest
}

public class Character : ScriptableObject
{
    public E_Race Race;
    public E_Class Class;
    public List<Die> Dice;
    public string Description;
    public string Name;
    public Sprite CharacterImage;
}
