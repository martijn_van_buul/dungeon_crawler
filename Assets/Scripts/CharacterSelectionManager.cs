﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelectionManager : MonoBehaviour
{
    [SerializeField]
    List<Character> availableCharacters;

    [SerializeField]
    Image characterImage;
    [SerializeField]
    TMP_Text raceClassText;
    [SerializeField]
    List<Image> diceImages;
    [SerializeField]
    List<TMP_Text> diceText;
    [SerializeField]
    TMP_Text descriptionText;
    [SerializeField]
    TMP_InputField nameInputField;

    [SerializeField]
    List<Sprite> diceColors;
    [SerializeField]
    Button startGameButton;
    [SerializeField]
    TMP_Text startButtonText;

    [SerializeField]
    Player player;

    private int characterIndex = 0;

    private void Start()
    {
        ShowCharacter();

        nameInputField.onValueChanged.AddListener(delegate { InputfieldChanged(); });
    }

    public void SelectNextCharacter()
    {
        characterIndex = (characterIndex + 1) % availableCharacters.Count;
        ShowCharacter();
    }

    public void SelectPreviousCharacter()
    {
        characterIndex = (characterIndex + (availableCharacters.Count - 1)) % availableCharacters.Count;
        ShowCharacter();
    }

    private void ShowCharacter()
    {
        Character selectedCharacter = availableCharacters[characterIndex];
        raceClassText.text = selectedCharacter.Race.ToString() + " " + selectedCharacter.Class.ToString();
        characterImage.sprite = selectedCharacter.CharacterImage;

        for (int i = 0; i < 3; i++)
        {
            if (i < selectedCharacter.Dice.Count)
            {
                Die die = selectedCharacter.Dice[i];

                diceImages[i].enabled = true;
                diceText[i].enabled = true;

                diceImages[i].sprite = diceColors[(int)die.GetColor()];
                diceText[i].text = die.GetFaceCount().ToString();
            }
            else
            {
                diceImages[i].enabled = false;
                diceText[i].enabled = false;
            }
        }

        descriptionText.text = selectedCharacter.Description;
    }

    public void StartGame()
    {
        if(nameInputField.text.Length > 0)
        {
            Character selectedCharacter = availableCharacters[characterIndex];

            player.SetDisplayName(nameInputField.text);
            player.SetDice(selectedCharacter.Dice);
            player.SetCasterImage(selectedCharacter.CharacterImage);

            SceneManager.LoadScene(3);
        }
    }

    private void InputfieldChanged()
    {
        if (nameInputField.text.Length > 0)
        {
            startGameButton.interactable = true;
            startButtonText.color = Color.white;
        }
        else
        {
            startGameButton.interactable = false;
            startButtonText.color = new Color(0.45f, 0.45f, 0.45f, 1);
        }
    }
}
