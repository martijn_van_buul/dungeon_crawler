﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DieVisual : MonoBehaviour
{
    [SerializeField]
    Image diceImage;
    [SerializeField]
    TMP_Text faceValueText;

    private Die myDice;

    public void Initialize(Die dice)
    {
        myDice = dice;

        diceImage.color = DieUtil.getDiceColor(myDice.GetColor());

        string values = "";
        foreach (int value in myDice.GetValues())
            values += " " + value;

        faceValueText.text = values;
    }
}
